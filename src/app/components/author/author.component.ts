import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { FormGroup, FormControl } from "@angular/forms";
import { FormBuilder, FormArray } from "@angular/forms";

@Component({
  selector: "app-author",
  templateUrl: "./author.component.html",
  styleUrls: ["./author.component.css"],
})
export class AuthorComponent implements OnInit {
  bookForm!: FormGroup;
  field = true;
  data: Array<any> = [];
  books: Array<any> = [];

  constructor() {}

    onSubmit() {
    this.data.push(this.bookForm.value);
    console.log(this.data);
    this.books.push(this.bookForm.value.bookname);
    console.log(this.books);
  }

  ngOnInit() {
    this.bookForm = new FormGroup({
      Name: new FormControl(),
      Surname: new FormControl(),
      Origin: new FormControl(),
      check: new FormControl(),
      bookname: new FormControl(),
    });
  }
}
