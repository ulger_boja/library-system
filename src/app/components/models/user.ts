export interface UserModel {
  email: string;
  password: string;
  name: string;
  username: string;
}
