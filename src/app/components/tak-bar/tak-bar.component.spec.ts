import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TakBarComponent } from './tak-bar.component';

describe('TakBarComponent', () => {
  let component: TakBarComponent;
  let fixture: ComponentFixture<TakBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TakBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TakBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
