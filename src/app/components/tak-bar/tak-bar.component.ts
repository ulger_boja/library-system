import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-tak-bar',
  templateUrl: './tak-bar.component.html',
  styleUrls: ['./tak-bar.component.css']
})
export class TakBarComponent implements OnInit {

  login(){
    this.route.navigateByUrl('/login');
  }
  create(){
    this.route.navigateByUrl('/author');
  }
  home(){
    this.route.navigateByUrl('/');
  }

  constructor( private route: Router) { }

  ngOnInit(): void {
  }

}
