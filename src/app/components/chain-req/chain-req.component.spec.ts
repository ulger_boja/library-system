import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainReqComponent } from './chain-req.component';

describe('ChainReqComponent', () => {
  let component: ChainReqComponent;
  let fixture: ComponentFixture<ChainReqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChainReqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainReqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
