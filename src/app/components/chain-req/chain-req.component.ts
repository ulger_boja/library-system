import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
@Component({
  selector: 'app-chain-req',
  templateUrl: './chain-req.component.html',
  styleUrls: ['./chain-req.component.css'],
})
export class ChainReqComponent implements OnInit {
  res!: Observable<{}>;
  constructor(private fs: AngularFirestore) {}

  ngOnInit() {
    this.res = this.fs
      .collection('users')
      .valueChanges()
      .pipe(mergeMap((user) => this.fs.collection('books').valueChanges()));
    console.log(this.res);
  }
}
