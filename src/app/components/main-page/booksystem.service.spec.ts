import { TestBed } from '@angular/core/testing';

import { BooksystemService } from './booksystem.service';

describe('BooksystemService', () => {
  let service: BooksystemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BooksystemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
