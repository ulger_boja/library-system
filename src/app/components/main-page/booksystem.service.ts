import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";


@Injectable({
  providedIn: 'root'
})
export class BooksystemService {

  constructor(private fs: AngularFirestore) { }

  getAllBooks(){
    return this.fs.collection('books').valueChanges()
  }

}
