import { Component, OnInit } from '@angular/core';
import { BooksystemService } from './booksystem.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  allBooks:  Array<any> = [];

  constructor(private books : BooksystemService) { }

  ngOnInit(): void {
    this.books.getAllBooks().subscribe(Response =>{
      this.allBooks = Response
      console.log(this.allBooks)
    })
  }

}
