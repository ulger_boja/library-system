import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-parallel-req',
  templateUrl: './parallel-req.component.html',
  styleUrls: ['./parallel-req.component.css'],
})
export class ParallelReqComponent implements OnInit {
  output1: any;
  output2: any;
  constructor(private fs: AngularFirestore) {}

  ngOnInit() {
    this.output1 = this.fs.collection('users').valueChanges();
    this.output2 = this.fs.collection('books').valueChanges();
    forkJoin([this.output1, this.output2]).subscribe((results) => {
      console.log(results);
    });
  }
}
