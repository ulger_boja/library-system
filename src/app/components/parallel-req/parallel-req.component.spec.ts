import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParallelReqComponent } from './parallel-req.component';

describe('ParallelReqComponent', () => {
  let component: ParallelReqComponent;
  let fixture: ComponentFixture<ParallelReqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParallelReqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParallelReqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
