import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { controllEmail } from '../../validators/emailValidator';
import { UserServiceService } from './user-service.service';
import {UserModel} from "../models/user";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  allUsers: UserModel[] = [];

  hide=true
  valid = false;
  logedin = false;
  emailValdiationRegex = new RegExp(/^.+\@.+\..+$/);

  constructor(private route: Router,
    private us : UserServiceService,) {}

  ngOnInit(): void {
    this.us.getAllUssers().subscribe((response: UserModel[]) =>{
      this.allUsers = response
      console.log(response)
    })
      this.us.getAllUssers().subscribe( response => {
      console.log(response)
    })
  }

  controllEmail(data: any) {
    console.log('trigger'+data)
    let email: string = data.email;
    if (email && email.indexOf('@') != -1) {
      let [_, domain] = email.split('@');
      if (domain.match(this.emailValdiationRegex)) {
        this.valid= false;
      }else{
        this.valid= true;
      }
    }
    this.valid= true;
  }

  onSubmit(data?: any,email?: string, password?: string) {
    this.allUsers.forEach((user: UserModel) => {
      if (user.email === email && user.password === password) {
      this.logedin = true;
      this.route.navigateByUrl('/author');
      } else {
        console.log("this user does not exist")
      }
    })
  }
}
