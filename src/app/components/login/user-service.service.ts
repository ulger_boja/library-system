import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserModel } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class UserServiceService {
  constructor(private fs: AngularFirestore) {}

  getAllUssers() {
    return this.fs.collection<UserModel>('users').valueChanges();
  }
}
