import { FormControl } from "@angular/forms";

export function controllEmail(data: any, control: FormControl) {
    let email: string = data.email;
    if (email && email.indexOf('@') != -1) {
      let [_, domain] = email.split('@');
      if (domain !== 'gmail.com') {
        return {
          emailDomain: {
            parseDomain: domain,
          },
        };
      }
      return null;
    }
    return null
  }
