// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCXoAaOQGqGLOuVQNsrdHS9QqNIi3CzscQ',
    authDomain: 'library-project-6337a.firebaseapp.com',
    projectId: 'library-project-6337a',
    storageBucket: 'library-project-6337a.appspot.com',
    messagingSenderId: '406837917993',
    appId: '1:406837917993:web:7c062df4d9444ef6825f38',
    measurementId: 'G-6XFD793PC2',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
